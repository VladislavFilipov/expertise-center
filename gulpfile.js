let gulp = require('gulp');
let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let autoprefixer = require('gulp-autoprefixer');
let cleanCss = require('gulp-clean-css');
let concat = require('gulp-concat');
let gulpIf = require('gulp-if');
let browserSync = require('browser-sync').create();
let htmlclean = require('gulp-htmlclean');
let rigger = require('gulp-rigger');
let uglify = require('gulp-uglify');
const babel = require('gulp-babel');

let config = {
    paths: {
        scss: './src/scss/**/*.scss',
        html: './src/html/**/*.html',
        js: './src/js/**/*.js'
    },
    output: {
        // htmlName: 'index.html',
        jsName: 'bundle.min.js',
        cssName: 'bundle.min.css',
        path: './public'
    },
    isDevelop: false
};

gulp.task('scss', () => {
    return gulp.src(config.paths.scss)
        .pipe(gulpIf(config.isDevelop, sourcemaps.init()))
        .pipe(sass())
        .pipe(concat(config.output.cssName))
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(gulpIf(!config.isDevelop, cleanCss()))
        .pipe(gulpIf(config.isDevelop, sourcemaps.write()))
        .pipe(gulp.dest(config.output.path))
        .pipe(browserSync.stream());
});

gulp.task('html', () => {
    return gulp.src(config.paths.html)
        // .pipe(gulpIf(config.isDevelop, sourcemaps.init()))
        .pipe(rigger())
        .pipe(gulpIf(config.isDevelop, htmlclean()))
        // .pipe(concat(config.output.htmlName))
        // .pipe(gulpIf(config.isDevelop, sourcemaps.write()))
        .pipe(gulp.dest(config.output.path))
        .pipe(browserSync.stream());
});

gulp.task('js', () => {
    return gulp.src(config.paths.js)
        .pipe(gulpIf(config.isDevelop, sourcemaps.init()))
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(concat(config.output.jsName))
        .pipe(gulpIf(config.isDevelop, sourcemaps.write()))
        .pipe(uglify())
        .pipe(gulp.dest(config.output.path))
        .pipe(browserSync.stream());
});

gulp.task('serve', () => {
    browserSync.init({
        server: {
            baseDir: config.output.path
        }
    });

    gulp.watch(config.paths.scss, gulp.series('scss'));
    gulp.watch(config.paths.html, gulp.series('html'));
    gulp.watch(config.paths.js, gulp.series('js'));
    // gulp.watch(config.paths.html).on('change', browserSync.reload);
    // gulp.watch(config.paths.js).on('change', browserSync.reload);
});

gulp.task('default', gulp.series('scss', 'html', 'js', 'serve'));