class List {
    constructor(elem) {
        elem.addEventListener('click', this.onTitleClick.bind(this));
    }

    onTitleClick(e) {
        let target;
        if (e.target.classList.contains('title'))
            target = e.target;
        else
            return;

        let
            contentHeight = parseFloat(getComputedStyle(target.nextElementSibling).height),
            containerHeight = parseFloat(getComputedStyle(target.parentNode).height),
            titleHeight = parseFloat(getComputedStyle(target).height);


        if (containerHeight > titleHeight) {
            target.parentNode.style.height = titleHeight + 'px';
        } else {
            target.parentNode.style.height = containerHeight + contentHeight + 'px';
        }
    }
}

// let Experts = new List(document.querySelector('.experts .list'))