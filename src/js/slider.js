class Slider {
    constructor(elem) {
        this.elem = elem;
        this.slide = 0;
        // this.sliderWidth = parseFloat(getComputedStyle(elem).width);
        this.moveBox = this.elem.querySelector('.move-box');
        this.btnBack = this.elem.querySelector('.btn-back');
        this.btnFwd = this.elem.querySelector('.btn-fwd');

        // console.log(this.btnFwd);
        this.btnBack.classList.add('hide');
        this.sliderLen = this.moveBox.children.length - 1;

        // let maxHeight = 0;
        // this.elem.querySelectorAll('.slide').forEach(slide => {
        //     console.log(getComputedStyle(slide).height);

        //     if (maxHeight < getComputedStyle(slide).height) {
        //         maxHeight = getComputedStyle(slide).height;
        //     }
        //     // maxHeight = maxHeight > getComputedStyle(slide).height ? getComputedStyle(slide).height : maxHeight
        // });

        // console.log(getComputedStyle(this.elem).height);

        // this.elem.style.heigth = getComputedStyle(this.moveBox).height;

        // console.log(getComputedStyle(this.elem).height);
        // console.log(this.elem.style.heigth);

        this.elem.querySelector('.btn-back').addEventListener('click', this.onButtonBackClick.bind(this));
        this.elem.querySelector('.btn-fwd').addEventListener('click', this.onButtonFwdClick.bind(this));
    }

    onButtonBackClick() {
        let sliderWidth = parseFloat(getComputedStyle(this.elem).width);
        this.slide = this.slide == 0 ? 0 : this.slide - 1;
        this.moveBox.style.transform = `translateX(-${this.slide * sliderWidth}px)`;

        this.btnFwd.classList.remove('hide');

        if (this.slide == 0)
            this.btnBack.classList.add('hide');
        else
            this.btnBack.classList.remove('hide');
    }

    onButtonFwdClick() {
        let sliderWidth = parseFloat(getComputedStyle(this.elem).width);
        this.slide = this.slide < this.sliderLen ? this.slide + 1 : this.sliderLen;
        this.moveBox.style.transform = `translateX(-${this.slide * sliderWidth}px)`;

        this.btnBack.classList.remove('hide');

        if (this.slide == this.sliderLen)
            this.btnFwd.classList.add('hide');
        else
            this.btnFwd.classList.remove('hide');
    }
}

let sliderOne = new Slider(document.querySelector('.about-expertise .slider'));
